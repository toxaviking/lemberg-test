<?php
$plugin = array(
  'single'          => TRUE,
  'title'           => t('Last nodes pane'),
  'description'     => t('Displays last 4 nodes'),
  'category'        => t('Custom panes'),
  'render callback' => 'last_four_nodes_pane_render',
  'all contexts'    => TRUE
);

function _lemberg_test_last_four_nodes(){
  $nodes_id = db_select('node','n')
    ->fields('n', array('nid'))
    ->condition('type', 'custom_type', '=')
    ->orderBy('created','DESC')
    ->range(0, 4)
    ->execute()
    ->fetchAllKeyed(0,0);
  return $nodes_id;
}

function last_four_nodes_pane_render($subtype, $conf, $args, $contexts){
  $nodes = node_load_multiple(_lemberg_test_last_four_nodes());
  $rendered_nodes = '';
  foreach ($nodes as $node){
    $rend_node_array = node_view($node,'teaser');
    $rendered_nodes .= drupal_render($rend_node_array);
  }
  $block = new stdClass();
  $block->content = $rendered_nodes;
  return $block;
}